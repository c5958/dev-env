#!/bin/bash
set -e

source .repos.sh

for repo in ${REPOS[*]}; do
  if [ ! -d "$repo" ]; then
    echo "git@gitlab.com:c5958/${repo}.git"
    git clone "git@gitlab.com:c5958/${repo}.git" $repo
    git -C $repo checkout develop
  else
    echo "${repo} already exists. Skipping."
  fi
done
