# dev-env

crawler service


## Getting started

To SetUp project in dev-env folder

```
./setup.sh
```

this will clone backend and nginx repos, and build docker images for them.

To Run app in dev-env folder

```
docker-compose up
```

To Run tests in dev-env folder

```
docker-compose run backend ./docker/run-tests.sh
```
